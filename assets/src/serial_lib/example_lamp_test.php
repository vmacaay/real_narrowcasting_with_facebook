<?php
// Ask Arduino if the LED is ON or OFF

$serial->deviceOpen();
$serial->sendMessage(chr(10)); // start transmission
$serial->sendMessage("2");
$serial->sendMessage(chr(13)); // end transmission
$read = $serial->readPort(); // waiting for reply
echo $read;
//We're done, so close the serial port again
$serial->deviceClose();