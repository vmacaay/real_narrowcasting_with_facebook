<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vmacaay
 * Date: 17-04-13
 * Time: 09:36
 * To change this template use File | Settings | File Templates.
 */
// Set acces to the Facebook API.
require 'src/facebook.php';
// Set you app credentals.

//$my_url = "http://localhost:8888/NarrowCastingFacebookWebservice/get_acces.php";
//
//session_start();
//
//
//$code = $_REQUEST["code"];
//// $scope=user_likes; is where we ask premission for likes.
//if(empty($code)) {
//  $_SESSION['state'] = md5(uniqid(rand(), TRUE)); // CSRF protection
//  $dialog_url = "https://www.facebook.com/dialog/oauth?client_id="
//    . $app_id . "&redirect_uri=" . urlencode($my_url) . "&state="
//    . $_SESSION['state'] . "&scope=user_likes";
//
//  header("Location: " . $dialog_url);
//}
//
//if($_SESSION['state'] && ($_SESSION['state'] === $_REQUEST['state'])) {
//  $token_url = "https://graph.facebook.com/oauth/access_token?"
//    . "client_id=" . $app_id . "&redirect_uri=" . urlencode($my_url)
//    . "&client_secret=" . $app_secret . "&code=" . $code;
//
//  $response = file_get_contents($token_url);
//  $params = null;
//  parse_str($response, $params);
//
//  $_SESSION['access_token'] = $params['access_token'];
//  //TODO write acces_token to database.
//
//  $graph_url = "https://graph.facebook.com/me?fields=id,name,movies?access_token=AAAH9uTOltZCIBAAMFXZAJxmOPgyCtxHHI3r4jZBr0F4mu2wJcsIikP2Y5kYM1Lm4mFpVYJNduKgxT0QotUIZAzcRZChax0NV1NxUVL0TZAvQZDZD";
//    //"https://graph.facebook.com/me?access_token="
//    //. $params['access_token'];
//  //echo($graph_url);
//  $user = json_decode(file_get_contents($graph_url));
//  //echo("Hello " . $user->name);
//  //print_r($user);
//}
//else {
//  echo("The state does not match. You may be a victim of CSRF.");
//}
?>

<!DOCTYPE html>
<html>
<head>
  <title>Real world narrowcasting APP</title>
  <link rel="stylesheet" href="css/reset.css" type="text/css" />
  <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css" />
  <script src="js/jquery.min.js"></script>

  <style>
    body {
      padding-top: 40px;
    }
    #main {
      margin-top: 80px;
      text-align: center;
    }
  </style>
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top">
  <div class="navbar-inner">
    <div class="container">
      <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="brand" href="#">Real world narrowcasting With Facebook</a>
      <div class="nav-collapse collapse">
        <!--        <ul class="nav">-->
        <!--          <li class="active"><a href="#">Home</a></li>-->
        <!--          <li><a href="#about">About</a></li>-->
        <!--          <li><a href="#contact">Contact</a></li>-->
        <!--        </ul>-->
      </div><!--/.nav-collapse -->
    </div>
  </div>
</div>

<div id="main" class="container">

  <?php if ($user): ?>
    <?php print_r($user); ?>
    <h3><?php echo($user->name); ?></h3>
    <img src="https://graph.facebook.com/<?php echo $user->username; ?>/picture">
    <h3>You Logged in!</h3>
  <?php else: ?>
    <strong><em>You are not Connected.</em></strong>
  <?php endif ?>
</div>
</body>
</html>
