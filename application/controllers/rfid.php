<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vmacaay
 * Date: 23-04-13
 * Time: 22:11
 * To change this template use File | Settings | File Templates.
 */

class RFID extends CI_Controller
{

  public function index()
  {
    $data = $this->_getRightUser();
    $data = $this->_getInterest($data);
    $this->load->view('rfid_check_page', $data);
  }

  public  function advert(){
    $data = $this->_getRightUser();
    $data = $this->_getInterest($data);
    $pics_amount = (count($data['movies']) -1);
    $random = rand(0,$pics_amount);

    $movie = $data['movies'][$random]['name'];
    $name = $data['first_name'];
    $movie_data = $this->_getMovieData($movie);
    $genre = $movie_data->genres[0];
    $data['images'] = $this->_getGenrePictures($genre);
    $data['based_on_poster'] = $movie_data->poster;
    $data['based_on_title'] = $movie;
    $data['genre'] =$genre;
    $this->_movieAdvertSelector($genre);



    // Movie line, Let Taylor call out the users name and a movie he liked
    $words = $this->_getGenreSentence($genre, $name, $movie);
    $file = $this->_createMp3File($words);
    $data['file'] = "../".$file;
    $this->load->view('advert_view', $data);
  }

  private function _getMovieData($movie) {

   // To get the we need a genre, so lets get it by the using the api provided by imdbapi.org
    $movie = urlencode($movie);
    $movie_call = "http://imdbapi.org/?title=".$movie."&type=json&plot=none";
    $movie_data = json_decode(file_get_contents($movie_call));

//    print_r($movie_data);
    return $movie_data[0];
  }

  private function _movieAdvertSelector($genre){
    // Available genres: Action, Comedy, Family, History, Mystery, Sci-Fi, War, Adventure, Crime
    // Fantasy, Horror, News, Sport, Western, Animation, Documentary, Film-Noir, Music, Reality-TV
    // Talk-Show, Biography, Drama, Game-Show, Musical, Romance, Thriller

    switch($genre) {
      case "Comedy, Action":
        //do shit
      break;
      case "":
        //do shit
      default:
       // echo "genre ";
    }
  }

  private  function _getRfid(){
    $file = '*';
    $dir = 'assets/assets_rfid/rfid_handeler/';

    $sorted_array = $this->_listdir_by_date($dir.$file);
    $rfid = end($sorted_array);
    $pos = strrpos($rfid, "USE_");
        if ($pos === false) {
        }else {
          $rfid = substr($rfid, 4, -4);



        }
    return $rfid;
  }

  public  function getRfidJson(){
      $user_rfid = array(
        'rfid' => $this->_getRfid(),
      );

      echo json_encode($user_rfid);


  }

  private function _listdir_by_date($pathtosearch)
  {
    foreach (glob($pathtosearch) as $filename) {
      $file_array[filectime($filename)] = basename($filename); // or just $filename
    }
    ksort($file_array);
    return $file_array;
  }

  private function _getGenreSentence($genre, $first_name, $movie){
    $sentence = "";
    switch($genre){
      case 'Action':
        $sentence = $first_name.",You Liked ".$movie."?, Time for the next action packed movie";
      break;

      case 'Comedy':
        $sentence = "In the mood for a laugh?  ".$first_name. ", like ".$movie.'you wil laugh for shure!';
        break;

      case 'Drama':
        $sentence = "Were you Moved by".$movie.", wait till you see these movies!";
        break;

      case 'Horror':
        $sentence = $first_name." Don't be scared! ".$movie."was a little creepy. These are creepy too";
        break;

      case 'Animation':
        $sentence = $first_name." if you enjoyed ".$movie." Then you will love these movies";
        break;

      default :
        $sentence = 'He '.$first_name.', If you liked, '. $movie .', Than you do not want to miss the following movies';
    }
     return $sentence;
  }

  private function _getGenrePictures($genre){
    $this->load->model('image_model');
    $data = $this->image_model->getImagesByGenre($genre);

    return $data;
  }

  private function _createMp3File($words) {
    // Google Translate API cannot handle strings > 100 characters
    $words = urlencode($words);

    // Name of the MP3 file generated using the MD5 hash
    $file = md5($words);
    // Save the MP3 file in this folder with the .mp3 extension
    $file = "assets/audio/" . $file . ".mp3";
    // If the MP3 file exists, do not create a new request
    if (!file_exists($file)) {
      $mp3 = file_get_contents('http://translate.google.com/translate_tts?ie=utf-8&tl=en&q=' . $words);
      file_put_contents($file, $mp3);
    }

    return $file;
  }

  private function _getRightUser() {
    $this->load->model('rfid_model');
    $user_rfid = $this->rfid_model->getUser($this->_getRfid());

    $data['user_name'] = $user_rfid->facebook_name;
    $data['user_id'] = $user_rfid->facebook_id;
    $data['first_name'] = $user_rfid->first_name;
    $data['last_name'] = $user_rfid->last_name;
    $data['access_token'] = $user_rfid->acces_token;
    $data['rfid'] = $this->_getRfid();
    return $data;
  }

  private function _getInterest($data){
    require 'assets/src/facebook.php';
    // TODO move these settings to generic class or config
    $facebook = new Facebook(array(
      'appId' => '560446853986290',
      'secret' => 'b2f15030716bb252f393cd8dc39b4460',
    ));

    $access_token = $data['access_token'];
    $movies = $facebook->api(
      '/me?fields=movies',
      'GET',
      array(
        'access_token' => $access_token
      )
    );
    $music = $facebook->api(
      '/me?fields=music',
      'GET',
      array(
        'access_token' => $access_token
      )
    );
    $television_shows = $facebook->api(
      '/me?fields=television',
      'GET',
      array(
        'access_token' => $access_token
      )
    );

    $data['movies'] = $movies['movies']['data'];
    //var_dump($movies['movies']['data']);
    $data['television_shows'] = $television_shows['television']['data'];
    $data['music'] = $music['music']['data'];
    return $data;
  }

}