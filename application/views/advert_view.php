<?php
/**
 * Created by JetBrains PhpStorm.
 * User: vmacaay
 * Date: 24-04-13
 * Time: 02:17
 * To change this template use File | Settings | File Templates.
 */
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>

  <title>Taylored advert buddy</title>
  <meta http-equiv="content-type" content="text/html; charset=UTF-8"/>

  <link rel="stylesheet" href="../assets/css/supersized.css" type="text/css" media="screen"/>
  <link rel="stylesheet" href="../assets/theme/supersized.shutter.css" type="text/css" media="screen"/>
  <link rel="stylesheet" href="../assets/css/bootstrap.min.css" type="text/css"/>
  <link rel="stylesheet" href="../assets/css/custom.css" type="text/css"/>

  <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
  <script type="text/javascript" src="../assets/js/jquery.easing.min.js"></script>

  <script type="text/javascript" src="../assets/js/supersized.3.2.7.min.js"></script>
  <script type="text/javascript" src="../assets/theme/supersized.shutter.min.js"></script>

  <script type="text/javascript">

    jQuery(function ($) {

      $.supersized({

        // Functionality
        slide_interval: 5000,		// Length between transitions
        transition: 1, 			// 0-None, 1-Fade, 2-Slide Top, 3-Slide Right, 4-Slide Bottom, 5-Slide Left, 6-Carousel Right, 7-Carousel Left
        transition_speed: 700,		// Speed of transition

        // Components
        slide_links: 'blank',	// Individual links for each slide (Options: false, 'num', 'name', 'blank')
        slides: [			// Slideshow Images
          <?php
         $images = $images['images'];
            foreach($images as $image){
              echo "{image : '../assets/images/bg/movies/".$genre."/";
              echo $image;
              echo "', title : 'Image Credit: Marvel'},\n";
            }
          ?>
        ]

      });
    });
    var currentRfid = "<?php echo $rfid; ?>";
    console.log(currentRfid);
    console.log('dit is de eerste gezette rfid');
    setInterval(function () {
      $.ajax({
        url: '../index.php/getrfidjson',
        type: 'POST',
        data: "type=string",
        dataType: 'json',
        success: function (result) {
          if (currentRfid == result['rfid']) {
            //do nothing
            console.log('current is gelijk aan binnenkomende');
          } else {
            currentRfid = result['rfid'];
            window.location.reload();
            //alert(currentRfid);
          }
        },
        error: function () {
          alert('error');
        }
      })
    }, 500)


  </script>

</head>

<style type="text/css">
  ul#demo-block {
    margin: 0 15px 15px 15px;
  }

  ul#demo-block li {
    margin: 0 0 10px 0;
    padding: 10px;
    display: inline;
    float: left;
    clear: both;
    color: #aaa;
    background: url('assets/images/img/bg-black.png');
    font: 11px Helvetica, Arial, sans-serif;
  }

  ul#demo-block li a {
    color: #eee;
    font-weight: bold;
  }
</style>

<body>
<div id="based_on_container">
  <h6>Advert based on:</h6>
  <h6><?php echo $based_on_title?></h6>
  <!--  <img src="--><?php //echo $based_on_poster;?><!--" alt="based_on_poster">-->
  <h6>For user:</h6>
  <img id="facebook_picture" src="https://graph.facebook.com/<?php echo $user_name; ?>/picture">
</div>

<audio autoplay="autoplay">
  <!--  <source src="audio/song.mp3" type="audio/mp3" />-->
  <source src="<? echo $file; ?>" type="audio/mp3"/>
</audio>

</body>
</html>
